#include <cstdlib>
#include <random>
#include <vector>
#include <chrono>
#include <unordered_map>
#include <fstream>
#include "board.h"
#include "move.h"
#define forstate for(int i = 0 ; i < 16 ; i++) for(int j = 0 ; j < 4 ; j++) for(int k = 0 ; k < 4 ; k++)
typedef std::unordered_map <unsigned, float> State;
State state[16][4][4];
std::default_random_engine engine;
std::uniform_int_distribution <int> dis(0,20);
std::vector<Move> record[10000 + 1];
std::vector<time_t> time_record[2];
inline void Computer(Board &board, const int dir, int &next_tile, Move &p)
{
    static int bag[12] = {1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3}, bag_index;
    static int location[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    static int next[4][4] = {{12, 13, 14, 15}, {0, 4, 8, 12}, {0, 1, 2, 3}, {3, 7, 11, 15}};
    if(dir == 4)
    {
        board = Board();
        bag_index = 0;
        shuffle(bag, bag + 12, engine);
        shuffle(location, location + 16, engine);
        while(bag_index < 9)
        {
            board.take_place(location[bag_index], bag[bag_index]);
            record[next_tile].push_back(Move(location[bag_index], bag[bag_index]));
            bag_index++;
        }
        next_tile = bag[9];
        return ;
    }
    else
    {
        shuffle(next[dir], next[dir] + 4, engine);
        for(int i = 0 ; i < 4 ; i++) if(board.take_place(next[dir][i], next_tile)) {p = Move(next[dir][i], next_tile); break;}
        if(next_tile < 4) bag_index++;
        if(bag_index == 12)
        {
            bag_index = 0;
            shuffle(bag, bag + 12, engine);
        }
        next_tile = bag[bag_index];
        if(board.maxtile() >= 7 && !dis(engine))
        {
            std::uniform_int_distribution <int> tile(4, board.maxtile()-3);
            next_tile = tile(engine);
        }
    }
}
inline void Player(Board &board, const int next_tile, Move &p)
{
    int dir = -1;
    Board tmp;
    float res, maxvalue;
    for(int i = 0 ; i < 4 ; i++)
    {
        tmp = board;
        if(tmp.slide(i) != -1)
        {
            res = tmp.get_tuple(state[next_tile], i);
            if(dir == -1 || res > maxvalue)
            {
                dir = i;
                maxvalue = res;
            }
        }
    }
    if(dir == -1) {p = Move(); return ;}
    board.slide(dir);
    p = Move(dir);
}
inline void initialize()
{
    engine.seed(time(NULL));
    forstate state[i][j][k].clear();
}
inline void input()
{
    int num, index;
    float value;
    std::ifstream fin("data", std::ios::binary);
    forstate
    {
        fin.read((char*)&num, sizeof(size_t));
        while(num--)
        {
            fin.read((char*)&index, sizeof(int));
            fin.read((char*)&value, sizeof(float));
            state[i][j][k][index] = value;
        }
    }
}
time_t get_time()
{
    auto now = std::chrono::system_clock::now().time_since_epoch();
    return std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
}
int main(int n, char **argv)
{
    //std::ios_base::sync_with_stdio(0);
    //std::cin.tie(0);
    int num_game = atoi(argv[1]), next_tile, dir;
    initialize();
    std::printf("Start Reading\n");
    input();
    std::printf("Reading Complete.\n");
    time_record[0].clear();
    time_record[1].clear();
    Board board;
    Move p;
    std::printf("Start Playing.\n");
    for(int i = 0 ; i < num_game ; i++)
    {
        record[i].clear();
        time_record[0].push_back(get_time());
        Computer(board, 4, next_tile = i, p);
        while(true)
        {
            Player(board, next_tile, p);
            if(p.slide() == -1) break;
            record[i].push_back(p);
            Computer(board, p.slide(), next_tile, p);
            record[i].push_back(p);
        }
        time_record[1].push_back(get_time());
    }
    std::printf("Complete Playing.\nStart Writing.\n");
    std::ofstream fout("result.txt");
    for(int i = 0 ; i < num_game ; i++)
    {
        fout << "0516310:N-Tuple@" << time_record[0][i] << "|";
        for(int j = 0 ; j < record[i].size() ; j++) fout << record[i][j];
        fout << "|N-Tuple@" << time_record[1][i] << "\n";
    }
    fout.close();
    std::printf("Complete Writing.\n");
    return 0;
}