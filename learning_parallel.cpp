#include <bits/stdc++.h>
#include <pthread.h>
#include "board.h"
#define thread_num 11
using namespace std;
typedef unsigned long long ULL;
typedef unordered_map <ULL, double> State;
unordered_map <ULL, long long> num[16];
State state[thread_num + 1][16], value[16];
default_random_engine engine;
uniform_int_distribution <int> dis(0,20);
pthread_t pthread[thread_num];
long long num_game;
class Data_for_computer
{
public:
	int bag[12] = {1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3}, bag_index;
	int location[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
	int next[4][4] = {{12, 13, 14, 15}, {0, 4, 8, 12}, {0, 1, 2, 3}, {3, 7, 11, 15}};
};
double Computer(Board &board, const int dir, int next_tile, Data_for_computer &data, int no);
ULL check(Board board, State &state)
{
	ULL index = board.get_index();
	if(state.find(index) != state.end()) return index;
	for(int i = 0 ; i < 3 ; i++)
	{
		board.rotate_clockwise();
		index = board.get_index();
		if(state.find(index) != state.end()) return index;
	}
	board.reverse();
	index = board.get_index();
	if(state.find(index) != state.end()) return index;
	for(int i = 0 ; i < 3 ; i++)
	{
		board.rotate_clockwise();
		index = board.get_index();
		if(state.find(index) != state.end()) return index;
	}
	return 0;
}
double Player(Board &board, const int next_tile, Data_for_computer &data, int no)
{
	ULL index;
	Board tmp;
	int dir = -1, ans;
	double res, maxvalue;
	for(int i = 0 ; i < 4 ; i++)
	{
		tmp = board;
		if(tmp.slide(i))
		{
			index = check(tmp, state[no][next_tile]);
			if(!index) res = 0;
			else res = state[no][next_tile][index];
			if(dir == -1 || res > maxvalue) 
			{
				dir = i;
				maxvalue = res;
			}
		}
	}
	if(dir == -1) return 0;
	index = check(board, state[no][next_tile]);
	if(!index) 
	{
		index = board.get_index();
		state[no][next_tile][index] = 0;
	}
	tmp = board;
	tmp.slide(dir);
	ans = Computer(tmp, dir, next_tile, data, no);
	if(ans != 0) ans -= board.count_score();
	ans -= state[no][next_tile][index];
	ans *= 0.1;
	state[no][next_tile][index] += ans;
	return maxvalue + tmp.count_score();
}
double Computer(Board &board, const int dir, int next_tile, Data_for_computer &data, int no)
{
	if(dir == 4)
	{
		data.bag_index = 0;
		shuffle(data.bag, data.bag + 12, engine);
		shuffle(data.location, data.location + 16, engine);
		while(data.bag_index++ < 9) board.take_place(data.location[data.bag_index-1], data.bag[data.bag_index-1]);
		return Player(board, data.bag[data.bag_index], data, no);
	}
	else
	{
		shuffle(data.next[dir], data.next[dir] + 4, engine);
		for(int i = 0 ; i < 4 ; i++) if(board.take_place(data.next[dir][i], next_tile)) break;
		if(next_tile < 4) data.bag_index++;
		if(data.bag_index == 12)
		{
			data.bag_index = 0;
			shuffle(data.bag, data.bag + 12, engine);
		}
		next_tile = data.bag[data.bag_index];
		if(board.maxtile() >= 7 && !dis(engine))
		{
			uniform_int_distribution <int> tile(4, board.maxtile()-3);
			next_tile = tile(engine);
		}
		return Player(board, next_tile, data, no);
	}
}
void initialize()
{
	engine.seed(time(NULL));
	for(int i = 0 ; i <= thread_num ; i++) for(int j = 0 ; j < 16 ; j++) state[i][j].clear();
	for(int i = 0 ; i < 16 ; i++)
	{
		value[i].clear();
		num[i].clear();
	}
}
void input()
{
	int num;
	ULL index;
	double value;
	ifstream fin("data", ios::binary);
	for(int i = 0 ; i < 16 ; i++)
	{
		fin.read((char*)&num, sizeof(size_t));
		printf("%d\n", num);
		while(num--)
		{
			fin.read((char*)&index, sizeof(ULL));
			fin.read((char*)&value, sizeof(double));
			for(int j = 0 ; j <= thread_num ; j++) state[j][i][index] = value;
			::value[i][index] = value;
			::num[i][index] = 0;
		}
	}
}
void combine()
{
	State::iterator it;
	ULL index;
	for(int i = 0 ; i <= thread_num ; i++)
	{
		for(int j = 0 ; j < 16 ; j++)
		{
			it = state[i][j].begin();
			for(; it != state[i][j].end() ; it++)
			{
				index = it -> first;
				if(::value[j].find(index) != value[j].end())
				{
					::value[j][index] += it -> second;
					::num[j][index] ++;
				}
				else
				{
					::value[j][index] = it -> second;
					::num[j][index] = 1;
				}
			}
		}
	}
}
void* parallel(void *s)
{
	int num = num_game, index = *(int*)s - 1;
	Board board;
	Data_for_computer data;
	while(num--) 
	{
		board = Board();
		Computer(board, 4, 0, data, index);
	}
}
int main(int n, char **argv)
{
	int tmp = 12;
	State::iterator it;
	size_t data_size;
	double value;
	printf("Database Preparing...\n");
	initialize();
	input();
	printf("Database ready.\n");
	num_game = atoll(argv[1]);
	printf("Start learning...\n");
	num_game /= thread_num + 1;
	for(int i = 0 ; i < thread_num ; i++) pthread_create(&pthread[i], 0, parallel, (void*)&i);
	parallel((void*)&tmp);
	for(int i = 0 ; i < thread_num ; i++) pthread_join(pthread[i], NULL);
	printf("Complete learning.\nStart writing...\n");
	combine();
	ofstream fout("data", ios::binary);
	for(int i = 0 ; i < 16 ; i++)
	{
		it = ::value[i].begin();
		fout.write((char*)&(data_size = ::value[i].size()), sizeof(size_t));
		for(; it != ::value[i].end() ; it++)
		{
			value = ::value[i][it->first] / ::num[i][it->first];
			fout.write((char*)&(it->first), sizeof(ULL));
			fout.write((char*)&value, sizeof(double));
		}
	}
	fout.close();
	printf("Complete writing.\n");
	return 0;
}